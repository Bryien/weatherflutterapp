package co.za.app.flutter_app.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import co.za.app.flutter_app.R;
import co.za.app.flutter_app.dtos.ListData;
import co.za.app.flutter_app.models.Favourite;

import java.text.DecimalFormat;
import java.util.List;

public class FavouriteListAdapter extends RecyclerView.Adapter<FavouriteListAdapter.FavouriteListViewHolder> {

	private List<Favourite>      favourites;
	private Context              context;
	// for item click listener
	private OnItemClickListener mOnItemClickListener;

	public interface OnItemClickListener {
		void onItemClick(View view, Favourite obj, int position);
	}
	public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
		this.mOnItemClickListener = mItemClickListener;
	}
	public FavouriteListAdapter(List<Favourite> favourites, Context context) {

		this.favourites = favourites;
		this.context = context;
	}

	@NonNull
	@Override
	public FavouriteListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

		View itemView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.row_favourite, parent, false);

		return new FavouriteListViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(@NonNull FavouriteListViewHolder holder, final int position) {

		final Favourite favourite= favourites.get(position);
		holder.name.setText(favourite.getName());
		holder.view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mOnItemClickListener.onItemClick(view, favourite, position);
			}
		});

	}

	@Override
	public int getItemCount() {

		return favourites.size();
	}

	public class FavouriteListViewHolder extends RecyclerView.ViewHolder {

		private TextView  name;
		private View view;

		public FavouriteListViewHolder(View view) {

			super(view);
			name = view.findViewById(R.id.txtFavName);
			this.view=view;

		}

	}

	public void setFavourites(List<Favourite> favourites) {
		this.favourites.clear();
		this.favourites = favourites;
		notifyDataSetChanged();
	}


}
