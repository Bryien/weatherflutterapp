package co.za.app.flutter_app.api;

import co.za.app.flutter_app.dtos.CurrentWeather;
import co.za.app.flutter_app.dtos.DaysForecast;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class WeatherClient extends Client {

	static WeatherContract weatherContract = builder.build().create(WeatherContract.class);

	public static WeatherContract getWeatherContract() {

		return weatherContract;
	}


	public interface WeatherContract {

		@GET("forecast")
		Call<DaysForecast> forecastWeather(@Query("lat") double lat, @Query("lon") double lon,
		                                         @Query("APPID") String appId);
		@GET("weather")
		Call<CurrentWeather> currentWeather(@Query("lat") double lat, @Query("lon") double lon,
		                                    @Query("APPID") String appId);
	}
}
