package co.za.app.flutter_app.dtos;

public class Main {
	double temp;
	double feelsLike;
	double temp_min;
	double temp_max;
	Object pressure;
	Object seaLevel;
	Object grndLevel;
	Object humidity;
	Object tempKf;

	public double getTemp() {

		return temp;
	}

	public void setTemp(double temp) {

		this.temp = temp;
	}

	public double getFeelsLike() {

		return feelsLike;
	}

	public void setFeelsLike(double feelsLike) {

		this.feelsLike = feelsLike;
	}


	public double getTemp_min() {

		return temp_min;
	}

	public void setTemp_min(double temp_min) {

		this.temp_min = temp_min;
	}

	public double getTemp_max() {

		return temp_max;
	}

	public void setTemp_max(double temp_max) {

		this.temp_max = temp_max;
	}

	public Object getPressure() {

		return pressure;
	}

	public void setPressure(Object pressure) {

		this.pressure = pressure;
	}

	public Object getSeaLevel() {

		return seaLevel;
	}

	public void setSeaLevel(Object seaLevel) {

		this.seaLevel = seaLevel;
	}

	public Object getGrndLevel() {

		return grndLevel;
	}

	public void setGrndLevel(Object grndLevel) {

		this.grndLevel = grndLevel;
	}

	public Object getHumidity() {

		return humidity;
	}

	public void setHumidity(Object humidity) {

		this.humidity = humidity;
	}

	public Object getTempKf() {

		return tempKf;
	}

	public void setTempKf(Object tempKf) {

		this.tempKf = tempKf;
	}
}
