package co.za.app.flutter_app.dtos;

public class City {
	int id;
	String name;
	Coord coord;
	String country;
	int timezone;
	int sunrise;
	int sunset;

	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public Coord getCoord() {

		return coord;
	}

	public void setCoord(Coord coord) {

		this.coord = coord;
	}

	public String getCountry() {

		return country;
	}

	public void setCountry(String country) {

		this.country = country;
	}

	public int getTimezone() {

		return timezone;
	}

	public void setTimezone(int timezone) {

		this.timezone = timezone;
	}

	public int getSunrise() {

		return sunrise;
	}

	public void setSunrise(int sunrise) {

		this.sunrise = sunrise;
	}

	public int getSunset() {

		return sunset;
	}

	public void setSunset(int sunset) {

		this.sunset = sunset;
	}
}
