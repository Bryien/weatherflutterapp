package co.za.app.flutter_app.utils;

import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import co.za.app.flutter_app.dtos.CurrentWeather;
import com.google.gson.Gson;

import java.util.List;
import java.util.Locale;

public class Utils {


	public static int buildTopImage(CurrentWeather weather, Context context) {

		if (weather == null) {
			return getResourseId(context, "forest_cloudy", "drawable", context.getPackageName());
		}
		String title = weather.getWeather().get(0).getMain();
		String currentWeather = title.toLowerCase();
		if (currentWeather.contains("cloud".toLowerCase())) {

			return getResourseId(context, "forest_cloudy", "drawable", context.getPackageName());

		} else if (currentWeather.contains("sun".toLowerCase())) {
			return getResourseId(context, "forest_sunny", "drawable", context.getPackageName());

		} else if (currentWeather.contains("clear".toLowerCase())) {

			return getResourseId(context, "forest_sunny", "drawable", context.getPackageName());

		} else if (currentWeather.contains("rain".toLowerCase())) {

			return getResourseId(context, "forest_rainy", "drawable", context.getPackageName());

		}
		return getResourseId(context, "forest_cloudy", "drawable", context.getPackageName());
	}


	public static int getResourseId(Context context, String pVariableName, String pResourcename, String pPackageName) throws RuntimeException {
		try {
			return context.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
		} catch (Exception e) {
			throw new RuntimeException("Error getting Resource ID.", e);
		}
	}

	public static int backgroundColor(CurrentWeather weather) {
		if (weather == null) {
			return Color.parseColor("#54717A");
		}
		String title = weather.getWeather().get(0).getMain();
		String currentWeather = title.toLowerCase();
		if (currentWeather.contains("cloud".toLowerCase())) {
			return Color.parseColor("#54717A");
		} else if (currentWeather.contains("sun".toLowerCase())) {
			return Color.parseColor("#47AB2F");
		} else if (currentWeather.contains("clear".toLowerCase())) {
			return Color.parseColor("#47AB2F");
		} else if (currentWeather.contains("rain".toLowerCase())) {
			return Color.parseColor("#57575D");
		}
		return Color.parseColor("#54717A");
	}

	public static String getCompleteAddressString(Context context,double LATITUDE, double LONGITUDE) {
		String strAdd = "";
		Geocoder geocoder = new Geocoder(context, Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
			if (addresses != null) {
				Address returnedAddress = addresses.get(0);
				StringBuilder strReturnedAddress = new StringBuilder("");

				for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
					strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
				}
				strAdd = strReturnedAddress.toString();
				Log.w("UTILS", strReturnedAddress.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.w("UTILS", "Canont get Address!");
		}
		return strAdd;
	}


	public static String toJson(Object object){
		Gson gson=new Gson();
		return gson.toJson(object);
	}


}
