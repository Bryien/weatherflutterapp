package co.za.app.flutter_app;

import android.app.Application;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class WeatherApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		Realm.init(this);
		RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
				.schemaVersion( 1 ) // Must be bumped when the schema changes
				.build();
		Realm.compactRealm( realmConfiguration );
		Realm.setDefaultConfiguration(realmConfiguration);

	}
}
