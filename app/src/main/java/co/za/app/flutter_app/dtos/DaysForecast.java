package co.za.app.flutter_app.dtos;

import java.util.List;

public class DaysForecast {
	String cod;
	int message;
	int cnt;
	List<ListData> list;
	City city;

	public String getCod() {

		return cod;
	}

	public void setCod(String cod) {

		this.cod = cod;
	}

	public int getMessage() {

		return message;
	}

	public void setMessage(int message) {

		this.message = message;
	}

	public int getCnt() {

		return cnt;
	}

	public void setCnt(int cnt) {

		this.cnt = cnt;
	}

	public List<ListData> getList() {

		return list;
	}

	public void setList(List<ListData> list) {

		this.list = list;
	}

	public City getCity() {

		return city;
	}

	public void setCity(City city) {

		this.city = city;
	}
}


















