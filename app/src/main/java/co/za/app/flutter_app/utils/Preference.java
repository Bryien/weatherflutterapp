package co.za.app.flutter_app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import co.za.app.flutter_app.dtos.CurrentWeather;
import co.za.app.flutter_app.dtos.DaysForecast;
import co.za.app.flutter_app.models.LocationPref;
import com.google.gson.Gson;

import static co.za.app.flutter_app.utils.Utils.toJson;

public class Preference {

	private static final String            CURRENT_WEATHER = "CURRENT_WEATHER";
	private static final String            DAYS_FORECAST   = "DAYS_FORECAST";
	private static final String            LOCATION_PREF   = "LOCATION_PREF";
	private              Context           context;
	private static       SharedPreferences settings;
	private static final String            PREFS_NAME      = "APP_PREFS";

	public Preference(Context context) {

		this.context = context;
		settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
	}

	public void saveCurrentWeather(CurrentWeather currentWeather) {

		SharedPreferences.Editor editor = settings.edit();
		String json = toJson(currentWeather);
		editor.putString(CURRENT_WEATHER, json);
		editor.apply();
	}

	public void saveForecastWeather(DaysForecast daysForecast) {

		SharedPreferences.Editor editor = settings.edit();
		String json = toJson(daysForecast);
		editor.putString(DAYS_FORECAST, json);
		editor.apply();
	}

	public void saveLocationPref(LocationPref locationPref) {

		SharedPreferences.Editor editor = settings.edit();
		String json = toJson(locationPref);
		editor.putString(LOCATION_PREF, json);
		editor.apply();
	}

	public CurrentWeather getCurrentWeather() {

		Gson gson = new Gson();
		settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		String json = settings.getString(CURRENT_WEATHER, null);
		if (json == null) {
			return null;
		}
		CurrentWeather currentWeather = gson.fromJson(json, CurrentWeather.class);

		return currentWeather;
	}

	public DaysForecast getDaysForecast() {

		Gson gson = new Gson();
		settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		String json = settings.getString(DAYS_FORECAST, null);
		if (json == null) {
			return null;
		}
		DaysForecast daysForecast = gson.fromJson(json, DaysForecast.class);

		return daysForecast;
	}


	public LocationPref getLocationPref() {

		Gson gson = new Gson();
		settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		String json = settings.getString(LOCATION_PREF, null);
		if (json == null) {
			return null;
		}
		LocationPref locationPref = gson.fromJson(json, LocationPref.class);

		return locationPref;
	}

}
